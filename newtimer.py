# -*- coding: utf-8 -*-
#!/usr/bin/env python3

'''
Example config file:

GPIO25(LED) 7:30-0:30
GPIO17(PUMP) 9:00-9:15, 11:00-11:30
'''

from datetime import datetime, time, timedelta
from time import sleep
from re import compile, search, match
import RPi.GPIO as GPIO
import signal

##### Functions:

def gpio_exit(sig=None, frame=None):
	print('\nCleaning GPIO...')
	GPIO.cleanup()
	exit(0)

def wait(sec):
	for i in range(sec, 0, -1):
		print('Waiting {0}s to status update. CTRL+C to quit'.format(i), end='\033[K\r')
		sleep(1)

def is_in_time(time1, time2, timenow=None):
	time_now = timenow or datetime.now().time()
	if time1 < time2:
		return time_now >= time1 and time_now <= time2
	else:
		return time_now >= time1 or time_now <= time2

class GpioObject():
	""" GPIO object """

	def __init__(self, config_string):
		gpiostr, timing = config_string.split(' ', 1) #Dzieliny nazwe i czasy wlaczenia
		self.gpio_nr, self.gpio_name = compile(r'GPIO(\d+)(\(.+\))?').match(gpiostr).groups() #Nr Pinu GPIO i "nazwa"
		self.times = compile(r'(\d{1,2}:\d{1,2})-(\d{1,2}:\d{1,2})').findall(timing) #zwraca liste krotek [('9:00', '9:15'), itd]
		GPIO.setup(int(self.gpio_nr), GPIO.OUT) #setupujemy port gpio na out

	def __str__(self):
		return self.gpio_name[1:-1].lower() #ucinamy nawiaski

	def __repr__(self):
		return self.gpio_name[1:-1].lower() #ucinamy nawiaski

	def is_active(self):
		for t in self.times: #iterujemy po liscie przedzialow godzin
			st = t[0].split(':') #start time (ST, ET)
			et = t[1].split(':') #end time (ST, ET)
			if is_in_time(time(int(st[0]), int(st[1])), time(int(et[0]), int(et[1]))): #przekazujemy czas start i end time(HH,MM)
				return(True, t)
		return(False, t) #jesli zadna iteracja nie znajdzie sie w przedziale, to zwracamy false ;)

##### Main Loop:
signal.signal(signal.SIGINT, gpio_exit)
signal.signal(signal.SIGTERM, gpio_exit)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

while True:
	try:
		last_read = open('conf.txt').readlines()
		if 'config_file' not in locals() or config_file != last_read:
			config_file = last_read
			print('\033[93mReading conf.txt. Updating objects!\x1b[0m', end='\033[K\r\n')
			gpio_objects = []
			for line in config_file:
				gpio_objects.append(GpioObject(line)) #wrzucamy cala linie z pliku do obiektu
			gpio_dict = {o.__str__():o for o in gpio_objects} #tworzymy slownik z aktywnymi obiektami gpio
	except:
		print('conf.txt file not found.')
		gpio_exit()

	print('{} Update:'.format(datetime.now().strftime('%H:%M:%S')), end='\033[K\r\n')
	for k,o in gpio_dict.items():
		status = o.is_active()
		if status[0]:
			GPIO.output(int(o.gpio_nr), GPIO.HIGH)
			print('\t\033[92m{}{}: Active\x1b[0m {}-{}'.format(o.gpio_nr, o.gpio_name, *status[1]))
		else:
			GPIO.output(int(o.gpio_nr), GPIO.LOW)
			print('\t\033[91m{}{}: NOT Active\x1b[0m'.format(o.gpio_nr, o.gpio_name))
	wait(59)